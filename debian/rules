#!/usr/bin/make -f
# -*- makefile -*-

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

%:
	dh $@

# This font is available in EOT, SVG, WOFF and WOFF2 formats
FONTAWESOME = /usr/share/fonts-font-awesome/fonts/fontawesome-webfont
NODEJSDIR = debian/node-webfont/usr/share/nodejs

# Note that all of these are removed in debian/clean
execute_before_dh_auto_build:
	ln -s $(FONTAWESOME).ttf is-ttf/pixel.ttf
	ln -s $(FONTAWESOME).woff is-ttf/pixel.woff
	ln -s $(FONTAWESOME).eot is-ttf/pixel.eot
	ln -s $(FONTAWESOME).eot is-eot/fontawesome-webfont.eot
	ln -s $(FONTAWESOME).woff is-eot/fontawesome-webfont.woff
	ln -s $(FONTAWESOME).ttf is-eot/fontawesome-webfont.ttf
	mkdir is-woff/fixture
	ln -s $(FONTAWESOME).woff is-woff/fixture/fontawesome-webfont.woff
	ln -s $(FONTAWESOME).woff2 is-woff/fixture/fontawesome-webfont.woff2
	mkdir is-woff2/test/fixture
	ln -s $(FONTAWESOME).woff is-woff2/test/fixture/fixture.woff
	ln -s $(FONTAWESOME).woff2 is-woff2/test/fixture/fixture.woff2
	mkdir wawoff2/test/fixtures
	ln -s $(FONTAWESOME).ttf wawoff2/test/fixtures/sample.ttf

	# Build the wawoff2 module
	cd wawoff2/src && make -f Makefile.Debian
	# Need to remove this build directory or rollup -c complains
	rm -rf wawoff2-build

execute_after_dh_auto_install:
	rm -f $(NODEJSDIR)/varstream/index.html

execute_before_dh_installman:
	chmod +x $(NODEJSDIR)/webfont/dist/cli.js
	help2man -N -n 'Convert a set of SVG files into a webfont in various formats' $(NODEJSDIR)/webfont/dist/cli.js > debian/webfont.1
	perl -i -pe 's/cli\.js/webfont/; s/CLI\.JS/WEBFONT/' debian/webfont.1

	chmod +x $(NODEJSDIR)/svgicons2svgfont/bin/svgicons2svgfont.js
	help2man -N -n 'Convert a set of SVG files into an SVG font' $(NODEJSDIR)/svgicons2svgfont/bin/svgicons2svgfont.js > debian/svgicons2svgfont.1
	perl -i -pe 's/svgicons2svgfont\.js/svgicons2svgfont/; s/SVGICONS2SVGFONT\.JS/SVGICONS2SVGFONT/' debian/svgicons2svgfont.1

override_dh_fixperms:
	dh_fixperms
	chmod +x $(NODEJSDIR)/webfont/dist/cli.js \
	    $(NODEJSDIR)/svgicons2svgfont/bin/*.js \
	    $(NODEJSDIR)/varstream/cli/*.js \
	    $(NODEJSDIR)/fast-xml-parser/cli.js \
	    $(NODEJSDIR)/ttf2eot/ttf2eot.js \
	    $(NODEJSDIR)/ttf2woff/ttf2woff.js \
	    $(NODEJSDIR)/wawoff2/bin/woff2_*.js
	chmod -x $(NODEJSDIR)/varstream/compare/*

override_dh_installdocs:
	dh_installdocs
	dh_nodejs_autodocs
	# debian/nodejs/docs overrides the automatic behaviour rather
	# than adding onto it, so we install extra documentation
	# manually
	install -m 644 fast-xml-parser/docs/* debian/node-webfont/usr/share/doc/node-fast-xml-parser/
	install -m 644 varstream/index.html debian/node-webfont/usr/share/doc/node-varstream/
	install -m 644 debian/node-webfont.README.Debian debian/node-webfont/usr/share/doc/node-wawoff2/
